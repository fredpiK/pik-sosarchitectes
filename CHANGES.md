### ChangeLog for SOS Architectes

#### 1.4
* Add options pages
* Create Custom Post Type 'Questions'
* Pages Created on Plugin Activation (My account|Step1|Step2|Step3)
* Template for Page 'My Account'
* Template for Pages 'Question' (Process)

#### 1.3
* Include auto update