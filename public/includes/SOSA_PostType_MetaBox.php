<?php
class SOSA_PostType_MetaBox extends K_AdminPageFramework_MetaBox {
		
	/*
	 * ( optional ) Use the setUp() method to define settings of this meta box.
	 */
	public function setUp() {
		
		/*
		 * ( optional ) Adds a contextual help pane at the top right of the page that the meta box resides.
		 */
		$this->addHelpText( 
			__( 'This text will appear in the contextual help pane.', 'admin-page-framework-demo' ), 
			__( 'This description goes to the sidebar of the help pane.', 'admin-page-framework-demo' )
		);
		
		 /*
		 * ( optional ) Adds setting fields into the meta box.
		 */
		$this->addSettingFields(
			array (
				'field_id'		=> 'status',
				'type'			=> 'radio',
				'title'			=> __( 'Status', 'pik-sosarchitectes' ),
				'description'	=> __( 'Change the status when Answer complete.', 'admin-page-framework-demo' ),
				'label' => array( 
					'new' 		=> __( 'New', 'pik-sosarchitectes' ),
					'pending' 	=> __( 'Pending', 'pik-sosarchitectes' ),
					'complete' 	=> __( 'Complete', 'pik-sosarchitectes' ),
				),
				'default' => 'new',
			),
			array(	
				'field_id' 		=> 'answer',
				'type' 			=> 'textarea',
				'title' 		=> __( 'Answer', 'pik-sosarchitectes' ),
				'rich' 			=> true,	// array( 'media_buttons' => false )  <-- a setting array can be passed. For the specification of the array, see http://codex.wordpress.org/Function_Reference/wp_editor
			),				
			array()
		);		
					
	}
	
	public function content_SOSA_PostType_MetaBox( $sContent ) {	// content_{instantiated class name}
		
		// Modify the output $sContent . '<pre>Insert</pre>'
		//$sInsert = "<p>" . sprintf( __( 'This text is inserted with the <code>%1$s</code> hook.', 'sos-architectes' ), __FUNCTION__ ) . "</p>";
		$sInsert = '';
		return $sInsert . $sContent;
		
	}
	
	public function validation_SOSA_PostType_MetaBox( $aInput, $aOldInput ) {	// validation_{instantiated class name}
	
		// You can check the passed values and correct the data by modifying them.
		// $this->oDebug->logArray( $aInput );
		return $aInput;
		
	}
	
}