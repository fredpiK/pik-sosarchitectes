<?php
class SOSA_PostType_MetaBox_User extends K_AdminPageFramework_MetaBox {
		
	/*
	 * ( optional ) Use the setUp() method to define settings of this meta box.
	 */
	public function setUp() {
		
		/*
		 * ( optional ) Adds a contextual help pane at the top right of the page that the meta box resides.
		 */
		$this->addHelpText( 
			__( 'This text will appear in the contextual help pane.', 'admin-page-framework-demo' ), 
			__( 'This description goes to the sidebar of the help pane.', 'admin-page-framework-demo' )
		);
		
		/*
		 * ( optional ) Set form sections - if not set, the system default section will be applied so you don't worry about it.
		 */
		$this->addSettingSections(
			array(
				'section_id'	=> 'selectors',
				'title'	=> __( 'Selectors', 'pik-sosarchitectes' ),
				'description'	=> __( 'These are grouped in the <code>selectors</code> section.', 'pik-sosarchitectes' ),
			),
			array(
				'section_id'	=> 'misc',
				'title'	=> __( 'MISC', 'pik-sosarchitectes' ),
				'description'	=> __( 'These are grouped in the <code>misc</code> section.', 'pik-sosarchitectes' ),
			)	
		);
		/*
		 * ( optional ) Adds setting fields into the meta box.
		 */
		$this->addSettingFields(
			array(
				'field_id'		=> 'user_title',
				'type'			=> 'text',
				'title'			=> __( 'User title', 'pik-sosarchitectes' ),
				'description'	=> __( 'Displayed on Customer personal page only. Should not be modified.', 'pik-sosarchitectes' ),
				'help'			=> 'This is help text.',
				'help_aside'	=> 'This is additional help text which goes to the side bar of the help pane.',
				'attributes'	=>	array(
					'readonly'	=>	'ReadOnly',
				),
			),
			array(
				'field_id'		=> 'date',
				'type'			=> 'text',
				'title'			=> __( 'Submitted the', 'pik-sosarchitectes' ),
				'description'	=> __( 'Displayed on Customer personal page only. Should not be modified.', 'pik-sosarchitectes' ),
				'attributes'	=>	array(
					'readonly'	=>	'ReadOnly',
				),
			),				
			array()
		);		
					
	}
	
	public function content_SOSA_PostType_MetaBox_User( $sContent ) {	// content_{instantiated class name}
		
		// Modify the output $sContent . '<pre>Insert</pre>'
		//$sInsert = "<p>" . sprintf( __( 'This text is inserted with the <code>%1$s</code> hook.', 'sos-architectes' ), __FUNCTION__ ) . "</p>";
		$sInsert = 'Add extra informations here';
		$sInsert .= 'i.e. Name, préname, et caetera';
		return $sContent . $sInsert;
		
	}
	
	public function validation_SOSA_PostType_MetaBox_User( $aInput, $aOldInput ) {	// validation_{instantiated class name}
	
		// You can check the passed values and correct the data by modifying them.
		// $this->oDebug->logArray( $aInput );
		return $aInput;
		
	}
	
}