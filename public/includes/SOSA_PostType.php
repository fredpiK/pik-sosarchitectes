<?php
class SOSA_PostType extends K_AdminPageFramework_PostType {
	
	public function start_SOSA_PostType() {	// start_{extended class name}
	
		// the setUp() method is too late to add taxonomies. So we use start_{class name} action hook.
	
		$this->setAutoSave( true );
		$this->setAuthorTableFilter( false );
		$this->addTaxonomy( 
			'sosa_question_taxonomy', // taxonomy slug
			array(
				'labels' 				=>	array(
									'name' 			=> __('Domain','pik-sosarchitectes'),
									'add_new_item' 	=> __('Add New Domain','pik-sosarchitectes'),
									'new_item_name' => __('New Domain','pik-sosarchitectes'),
									'all_items'		=> __('All Domains', 'pik-sosarchitectes'),
											),
				'show_ui' 				=> 	true,
				'show_tagcloud' 		=> 	false,
				'hierarchical' 			=> 	true,
				'show_admin_column' 	=> 	true,
				'show_in_nav_menus' 	=> 	true,
				'show_table_filter' 	=> 	true,	// framework specific key
				'show_in_sidebar_menus' => 	true,	// framework specific key
			)
		);

		$this->setFooterInfoLeft( '<br />Custom Text on the left hand side.' );
		$this->setFooterInfoRight( '<br />Custom text on the right hand side' );
			
		add_filter( 'the_content', array( $this, 'replyToPrintOptionValues' ) );	
	
	}
	
	/*
	 * Callback methods
	 */
	public function columns_sosa_question( $aHeaderColumns ) {	// columns_{post type slug}
		
		return array_merge( 
			$aHeaderColumns,
			array(
				'cb'				=> '<input type="checkbox" />',	// Checkbox for bulk actions. 
				'title'				=> __( 'Title', 'pik-sosarchitectes' ),		// Post title. Includes "edit", "quick edit", "trash" and "view" links. If $mode (set from $_REQUEST['mode']) is 'excerpt', a post excerpt is included between the title and links.
				'date'				=> __( 'Date', 'pik-sosarchitectes' ), 	// The date and publish status of the post. 
				'id'				=> __( 'ID', 'pik-sosarchitectes' ),
			)			
		);
		
	}
	public function sortable_columns_sosa_question( $aSortableHeaderColumns ) {	// sortable_columns_{post type slug}
		
		return $aSortableHeaderColumns;
		
	}	
	public function cell_sosa_question_id( $sCell, $iPostID ) {	// cell_{post type}_{column key}
		
		return "ID : {$iPostID}";
		
	}
	
	public function replyToPrintOptionValues( $sContent ) {
		
		if ( ! isset( $GLOBALS['post']->ID ) || get_post_type() != 'sosa_question' ) return $sContent;
			
		// 1. To retrieve the meta box data	- get_post_meta( $post->ID ) will return an array of all the meta field values.
		// or if you know the field id of the value you want, you can do $value = get_post_meta( $post->ID, $field_id, true );
		$iPostID = $GLOBALS['post']->ID;
		$aPostData = array();
		foreach( ( array ) get_post_custom_keys( $iPostID ) as $sKey ) 	// This way, array will be unserialized; easier to view.
			$aPostData[ $sKey ] = get_post_meta( $iPostID, $sKey, true );
		
		// 2. To retrieve the saved options in the setting pages created by the framework - use the get_option() function.
		// The key name is the class name by default. The key can be changed by passing an arbitrary string 
		// to the first parameter of the constructor of the AdminPageFramework class.		
		$aSavedOptions = get_option( 'SOSA_MenuPages' );
			
		return "<h3>" . __( 'Saved Meta Field Values', 'pik-sosarchitectes' ) . "</h3>" 
			. $this->oDebug->getArray( $aPostData )
			. "<h3>" . __( 'Saved Setting Options', 'pik-sosarchitectes' ) . "</h3>" 
			. $this->oDebug->getArray( $aSavedOptions );

	}	
	
}