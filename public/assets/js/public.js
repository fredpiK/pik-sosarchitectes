(function ( $ ) {
	"use strict";

	$(function () {

		// Place your public-facing JavaScript here
		$('.open_modal').magnificPopup({
			type: 'inline',
			preloader: false
		});

	});
	

}(jQuery));