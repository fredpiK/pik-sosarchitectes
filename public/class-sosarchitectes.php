<?php
/**
 * piK - SOS Architectes
 *
 * @package   Sosarchitectes
 * @author    Fred Pastel <contact@frederic-pastel.com>
 * @license   GPL-2.0+
 * @link      http://example.com
 * @copyright 2014 Fred Pastel - piK
 */
define( 'PUBLIC_DIRNAME', dirname( __FILE__) );

class Sosarchitectes {

	/**
	 * Plugin version, used for cache-busting of style and script file references.
	 *
	 * @since   1.0.0
	 *
	 * @var     string
	 */
	const VERSION = '1.0.0';

	/**
	 * Unique identifier for your plugin.
	 *
	 * @since    1.0.0
	 *
	 * @var      string
	 */
	protected $plugin_slug = 'pik-sosarchitectes';

	/**
	 * Instance of this class.
	 *
	 * @since    1.0.0
	 *
	 * @var      object
	 */
	protected static $instance = null;

	/**
	 * Initialize the plugin by setting localization and loading public scripts
	 * and styles.
	 *
	 * @since     1.0.0
	 */
	private function __construct() {

		// Load plugin text domain
		add_action( 'init', array( $this, 'load_plugin_textdomain' ) );

		// Activate plugin when new blog is added
		add_action( 'wpmu_new_blog', array( $this, 'activate_new_site' ) );

		// Load public-facing style sheet and JavaScript.
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

		/* Define custom functionality.
		 * Refer To http://codex.wordpress.org/Plugin_API#Hooks.2C_Actions_and_Filters
		 */
		//add_action( '@TODO', array( $this, 'action_method_name' ) );
		//add_filter( '@TODO', array( $this, 'filter_method_name' ) );
		
		//register shortcodes
		add_action( 'init', array( $this, 'sosa_register_shortcodes_action' ) );
		
		//pages templates
		add_filter( 'template_include', array( $this, 'sosa_plugin_templates_filter' ) );
		
		// create 'sosa_question' post type.
		include_once( PUBLIC_DIRNAME . '/includes/SOSA_PostType.php' );
		new SOSA_PostType( 	
			'sosa_question',
			array(			
				'labels'				=> array(
					'name' 					=> __('Questions','pik-sosarchitectes'),
					'all_items'				=> __('Questions','pik-sosarchitectes'),
					'singular_name' 		=> __('Question','pik-sosarchitectes'),
					'add_new' 				=> __('Add new','pik-sosarchitectes'),
					'add_new_item' 			=> __('Add Question','pik-sosarchitectes'),
					'edit' 					=> __('Edit','pik-sosarchitectes'),
					'edit_item' 			=> __('Edit Question','pik-sosarchitectes'),
					'new_item' 				=> __('New Question','pik-sosarchitectes'),
					'view' 					=> __('View','pik-sosarchitectes'),
					'view_item' 			=> __('View Question','pik-sosarchitectes'),
					'search_items' 			=> __('Search Question','pik-sosarchitectes'),
					'not_found' 			=> __('No Question found','pik-sosarchitectes'),
					'not_found_in_trash' 	=> __('No Question found in trash','pik-sosarchitectes')
				),
				'public' 				=> true,
				'menu_position' 		=> 110,
				'supports' 				=> array( 'title', 'editor' ), // 'supports' => array( 'title', 'editor', 'comments', 'thumbnail' ),	// 'custom-fields'
				'taxonomies' 			=> array( 'sosa_question_taxonomy' ),
				'has_archive' 			=> true,
				'show_admin_column' 	=> false,	// this is for custom taxonomies to automatically add the column in the listing table.
				'menu_icon'	=> 'dashicons-groups'
			)
		);
		// Add fields in the taxonomy page -- custom type 'sosa_question'
		//include_once( ADMIN_DIRNAME . '/includes/SOSA_TaxonomyField.php' );
		//new SOSA_TaxonomyField( 'sosa_question_taxonomy' );		// taxonomy slug
		/* Create a meta box with form fields */
		include_once( PUBLIC_DIRNAME . '/includes/SOSA_PostType_MetaBox.php' );	
		new SOSA_PostType_MetaBox(
			'post_meta_box',	// meta box ID
			__( 'Answer manager', 'pik-sosarchitectes' ),	// title
			array( 'sosa_question' ),	// post type slugs: post, page, etc.
			'normal',	// context (what kind of metabox this is)
			'default'	// priority
		);
		include_once( PUBLIC_DIRNAME . '/includes/SOSA_PostType_MetaBox_user.php' );	
		new SOSA_PostType_MetaBox_User(
			'post_meta_box_user',	// meta box ID
			__( 'User Information', 'pik-sosarchitectes' ),	// title
			array( 'sosa_question' ),	// post type slugs: post, page, etc.
			'normal',	// context (what kind of metabox this is)
			'default'	// priority
		);
		
		// Gravity Forms
		//add_filter('gform_pre_render', array( $this, 'sosa_populate_pricerules') );
		//add_filter('gform_pre_submission_filter', array( $this, 'sosa_populate_pricerules') );
		//add_filter('gform_admin_pre_render', array ( $this, 'sosa_populate_pricerules') );
		//add_action('gform_after_submission', array ( $this, 'sosa_after_submission' ), 10, 2);
	}

	/**
	 * Return the plugin slug.
	 *
	 * @since    1.0.0
	 *
	 *@return    Plugin slug variable.
	 */
	public function get_plugin_slug() {
		return $this->plugin_slug;
	}

	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Fired when the plugin is activated.
	 *
	 * @since    1.0.0
	 *
	 * @param    boolean    $network_wide    True if WPMU superadmin uses
	 *                                       "Network Activate" action, false if
	 *                                       WPMU is disabled or plugin is
	 *                                       activated on an individual blog.
	 */
	public static function activate( $network_wide ) {

		if ( function_exists( 'is_multisite' ) && is_multisite() ) {

			if ( $network_wide  ) {

				// Get all blog ids
				$blog_ids = self::get_blog_ids();

				foreach ( $blog_ids as $blog_id ) {

					switch_to_blog( $blog_id );
					self::single_activate();
				}

				restore_current_blog();

			} else {
				self::single_activate();
			}

		} else {
			self::single_activate();
		}

	}

	/**
	 * Fired when the plugin is deactivated.
	 *
	 * @since    1.0.0
	 *
	 * @param    boolean    $network_wide    True if WPMU superadmin uses
	 *                                       "Network Deactivate" action, false if
	 *                                       WPMU is disabled or plugin is
	 *                                       deactivated on an individual blog.
	 */
	public static function deactivate( $network_wide ) {

		if ( function_exists( 'is_multisite' ) && is_multisite() ) {

			if ( $network_wide ) {

				// Get all blog ids
				$blog_ids = self::get_blog_ids();

				foreach ( $blog_ids as $blog_id ) {

					switch_to_blog( $blog_id );
					self::single_deactivate();

				}

				restore_current_blog();

			} else {
				self::single_deactivate();
			}

		} else {
			self::single_deactivate();
		}

	}

	/**
	 * Fired when a new site is activated with a WPMU environment.
	 *
	 * @since    1.0.0
	 *
	 * @param    int    $blog_id    ID of the new blog.
	 */
	public function activate_new_site( $blog_id ) {

		if ( 1 !== did_action( 'wpmu_new_blog' ) ) {
			return;
		}

		switch_to_blog( $blog_id );
		self::single_activate();
		restore_current_blog();

	}

	/**
	 * Get all blog ids of blogs in the current network that are:
	 * - not archived
	 * - not spam
	 * - not deleted
	 *
	 * @since    1.0.0
	 *
	 * @return   array|false    The blog ids, false if no matches.
	 */
	private static function get_blog_ids() {

		global $wpdb;

		// get an array of blog ids
		$sql = "SELECT blog_id FROM $wpdb->blogs
			WHERE archived = '0' AND spam = '0'
			AND deleted = '0'";

		return $wpdb->get_col( $sql );

	}

	/**
	 * Fired for each blog when the plugin is activated.
	 *
	 * @since    1.0.0
	 */
	private static function single_activate() {
		
		//create page for user account if it doesn't exist
		$aPluginPages = array(
			'account' 	=> 	array(
				'slug'		=>	'mon-compte',
				'title'		=>	__('My account', 'pik-sosarchitectes'),
				'content'	=> 	__('Default page content', 'pik-sosarchitectes')
			),
			'step1'		=>	array(
				'slug'		=>	'step-1',
				'title'		=>	__('Step1', 'pik-sosarchitectes'),
				'content'	=> 	'[sosa_select_domain]'
			),
			'step2'		=>	array(
				'slug'		=>	'step-2',
				'title'		=>	__('Step2', 'pik-sosarchitectes'),
				'content'	=> 	'[sosa_question_form]',
				'parent'	=> 'step-1'
			),
			'step3'		=>	array(
				'slug'		=>	'step-3',
				'title'		=>	__('Step3', 'pik-sosarchitectes'),
				'content'	=> 	'[sosa_checkout_form]',
				'parent'	=> 'step-1'
			),
			
		);
		
		foreach ( $aPluginPages as $page ){
			$args = array(
				'post_content'  	=> $page['content'],
				'post_name'      	=> $page['slug'],
				'post_title'     	=> $page['title'],
				'post_status'    	=> 'publish',
				'post_type'      	=> 'page',
				'comment_status' 	=> 'closed'
			);
			if ($page['parent']){
				$oParentPage = get_page_by_path( $page['parent'] );
				$args = $oParentPage ? array_merge( array ( 'post_parent' => $oParentPage->ID ), $args ) : $args;
			}
			$sSlug = $page['parent'] ? $page['parent'] . '/' . $page['slug'] : $page['slug'];
			$oPage = get_page_by_path( $sSlug );
			if ( $oPage ) {
				// Page exists
	    		$aOldEntries = array(
	    			'ID' 		=> $oPage->ID,
	    			'content' 	=> $oPage->post_content,
	    			'title'		=> $oPage->post_title
	    		);
	    		$args = array_merge( $aOldEntries, $args );
	    	}
	    	// create or update Page
	    	$id = wp_insert_post( $args );
		}
		
		// define plugin settings default values
		if ( get_option( 'sosa_settings_ar' ) === false )
   		{
      		$options_array['plugin_pages'] = $aPluginPages;
			
			add_option( 'sosa_settings_ar', $options_array );
   		}
		
		flush_rewrite_rules();
			        
	}

	/**
	 * Fired for each blog when the plugin is deactivated.
	 *
	 * @since    1.0.0
	 */
	private static function single_deactivate() {
		
		$options = get_option( 'sosa_settings_ar' );

		// delete created Pages
		/*
		$aPluginPages = $options['plugin_pages'];
		foreach ($aPluginPages as $page){
			
			$oPage = get_page_by_path( $page['slug'] );
			if ( $oPage ) {
			    $iPageID = $oPage->ID;
			    wp_delete_post( $iPageID, true );
			}

		}

		delete_option( 'sosa_settings_ar' );
		*/
		
		flush_rewrite_rules();
		
	}

	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		$domain = $this->plugin_slug;
		$locale = apply_filters( 'plugin_locale', get_locale(), $domain );
		$plugin_lang_path = basename( plugin_dir_path( dirname( __FILE__ ) ) ) . '/languages/' ;
		load_plugin_textdomain( $domain, false, $plugin_lang_path );
	}

	/**
	 * Register and enqueue public-facing style sheet.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {
		wp_enqueue_style( $this->plugin_slug . '-plugin-styles', plugins_url( 'assets/css/public.css', __FILE__ ), array(), self::VERSION );
		wp_enqueue_style( $this->plugin_slug . '-modal-styles', plugins_url( 'assets/js/modal/magnific-popup.css', __FILE__ ), array(), self::VERSION );
		wp_enqueue_style( $this->plugin_slug . '-upload-styles', plugins_url( 'assets/js/uploadifive/uploadifive.css', __FILE__ ), array(), self::VERSION );
	}

	/**
	 * Register and enqueues public-facing JavaScript files.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		wp_enqueue_script( $this->plugin_slug . '-plugin-script', plugins_url( 'assets/js/public.js', __FILE__ ), array( 'jquery' ), self::VERSION );
		wp_enqueue_script( $this->plugin_slug . '-modal-script', plugins_url( 'assets/js/modal/jquery.magnific-popup.min.js', __FILE__ ), array( 'jquery' ), self::VERSION );
		wp_enqueue_script( $this->plugin_slug . '-upload-script', plugins_url( 'assets/js/uploadifive/jquery.uploadifive.js', __FILE__ ), array( 'jquery' ), self::VERSION );
	}

	/**
	 * NOTE:  Actions are points in the execution of a page or process
	 *        lifecycle that WordPress fires.
	 *
	 *        Actions:    http://codex.wordpress.org/Plugin_API#Actions
	 *        Reference:  http://codex.wordpress.org/Plugin_API/Action_Reference
	 *
	 * @since    1.0.0
	 */
	public function action_method_name() {
		// @TODO: Define your action hook callback here
	}

	/**
	 * NOTE:  Filters are points of execution in which WordPress modifies data
	 *        before saving it or sending it to the browser.
	 *
	 *        Filters: http://codex.wordpress.org/Plugin_API#Filters
	 *        Reference:  http://codex.wordpress.org/Plugin_API/Filter_Reference
	 *
	 * @since    1.0.0
	 */
	public function filter_method_name() {
		// @TODO: Define your filter hook callback here
	}
	
	public function sosa_plugin_templates_filter( $page_template ){
		global $post;
		
		//var_dump($post);
		
		$options = get_option( 'sosa_settings_ar' );
		$aPluginPages = $options['plugin_pages'];
		
		foreach ( $aPluginPages as $page ){
			$sSlug = $page['slug'];
			
			if ( $post->post_name == $sSlug ) {
				
				// checks if the file exists in the theme first
				if ( $theme_file = locate_template( array( 'page-' . $sSlug . '.php' ) ) ) {
					$page_template = $theme_file;
				} else {
					$page_template = plugin_dir_path( dirname( __FILE__ ) ) . '/templates/page-' . $sSlug . '.php';
				}
				
			}
		}
		
    	return $page_template;
    	
	}
	
	// SHORTCODES *****************
	public function sosa_register_shortcodes_action(){
		add_shortcode( 'sosa_user_block', array( $this, 'sosa_user_block_shortcode' ) );
		add_shortcode( 'sosa_select_domain', array( $this, 'sosa_select_domain_shortcode' ) );
		add_shortcode( 'sosa_question_form', array( $this, 'sosa_question_form_shortcode' ) );
		add_shortcode( 'sosa_checkout_form', array( $this, 'ssa_checkout_form_shortcode' ) );
	}
	
	public function sosa_checkout_form_shortcode( $atts ){
		
	}
	
	public function sosa_question_form_shortcode( $atts ){
		if (! $_REQUEST['selected_domain_id']) return;
		$iTaxID = $_REQUEST['selected_domain_id'];
		
		$sTaxonomy = 'sosa_question_taxonomy';
		$oTerm = get_term( $iTaxID, $sTaxonomy );

		//var_dump($oTerm);
		ob_start();
		?>
		<div id="description-block">
		<?php echo $oTerm->description; ?>
		</div>
		<div id="user-block">
			<h3><?php _e('Your personal informations','pik-sosarchitectes'); ?></h3>
			<ul>
		<?php
		$iUserID = get_current_user_id();
		if ($iUserID){
			$oUserData = get_user_meta($iUserID);
			
			// array model for retrieving : Name / Field key
			$aKeys = array(
				array( __('Organisation','pik-sosarchitectes'), 'organisation' ),
				array( __('Last Name','pik-sosarchitectes'), 'last_name' ),
				array( __('First Name','pik-sosarchitectes'), 'first_name' ),
				array( __('City','pik-sosarchitectes'), 'city' ),
				array( __('Adress','pik-sosarchitectes'), 'adress' ),
				array( __('Zipcode','pik-sosarchitectes'), 'zip' ),
				array( __('Phone','pik-sosarchitectes'), 'phone' ),
				array( __('Mobile phone','pik-sosarchitectes'), 'phone2' ),
				array( __('Email','pik-sosarchitectes'), 'user_email' ),
				array( __('Status','pik-sosarchitectes'), 'status' ),
			);
			
			foreach ( $aKeys as $aK ){
				$sTitle = $aK[0];
				$sKey = $aK[1];
				if ( isset ($oUserData[$sKey][0]) ){
		?>
				<li><?php echo $sTitle . ': ' . $oUserData[$sKey][0];?></li>
		<?php
				}
			}	
		} else {
			return 'Not logged';
		}
		?>
			</ul>
		</div>
		<?php
		
		?>
		<div id="question-block">
			<h3><?php _e('Your question','pik-sosarchitectes'); ?></h3>
			<?php
			// get plugin options -- retrieve Form name
			//$aSOSAOptions = get_option( 'SOSA_MenuPages' );
			//$sFormName = $aSOSAOptions['sosa_settings_diverse']['question_form'];
			// add current selected taxonomy ID in an hidden field
			//$aDynamicFields = array(
			//	'sosa_cat_id'		=> 	$iTaxID,
			//);
			
			//gravity_form($sFormName, false, false, false, $aDynamicFields, true );
			
			$options = get_option( 'sosa_settings_ar' );
			$aPluginPages = $options['plugin_pages'];
			if ( $aPluginPages['step3']['parent'] ){
				$oTargetPage = get_page_by_path( $aPluginPages['step3']['parent'] . '/' . $aPluginPages['step3']['slug'] );
			} else {
				$oTargetPage = get_page_by_path( $aPluginPages['step3']['slug'] );
			}
			if ( ! $oTargetPage ) return 'Missing $oTargetPage';
			$sTargetPageLink = get_permalink( $oTargetPage->ID );
			
			$aSOSAOptions = get_option( 'SOSA_MenuPages' );
			//$sFormName = $aSOSAOptions['sosa_settings_diverse']['question_form'];
			//$sFieldInputName = $aSOSAOptions['sosa_settings_diverse']['pricerules_dynamic_field'];
			$aPriceRules = $aSOSAOptions['sosa_settings_pricerules'];
			?>
			<form id="sosa_question_form" method="POST" action="<?php echo $sTargetPageLink; ?>">
				<?php wp_nonce_field( 'wp_nonce', 'sosarchitectes' ); ?>
				<input id="selected_domain_id" name="selected_domain_id" type="hidden" value="<?php echo $iTaxID; ?>"/>
				<input id="user_id" name="user_id" type="hidden" value="<?php echo $iUserID; ?>"/>
				<fieldset>
				<span class="title"><?php _e('Delay','pik-sosarchitectes');?></span>
				<span class="choice">
			<?php
			$id = 0;
			foreach ($aPriceRules as $aRule){
			?>
				
				<label for="sosa_delay_<?php echo $id;?>"><input type="radio" id="sosa_delay_<?php echo $id;?>" name="sosa_delay" value="<?php echo $aRule['price'];?>" required/><?php echo $aRule['name'] . ' | ' . $aRule['price'] . '€ HT';?></label>
			<?php
				$id++;
			}	
			?>
				</span>
				</fieldset>
				<fieldset>
					<span class="title"><label for="sosa_question_title"><?php _e('Title','pik-sosarchitectes');?></label></span>
					<span class="choice">
						<input type="text" id="sosa_question_title" id="sosa_question_title" required/>
					</span>
				</fieldset>
				<fieldset>
					<span class="title"><label for="sosa_question_text"><?php _e('Question','pik-sosarchitectes');?></label></span>
					<span class="choice">
						<textarea id="sosa_question_text" id="sosa_question_text" required ></textarea>
					</span>
				</fieldset>
				<fieldset>
					<span class="title"><?php _e('Files','pik-sosarchitectes');?></span>
					<span class="choice">
						<input id="file_upload" type="file" name="file_upload" />
					</span>
				</fieldset>
				<input type="submit" value="<?php _e('Validate','pik-sosarchitectes');?>"/>
			</form>
			<script type="text/javascript">
		        
		        jQuery(function($) {
		            $('#file_upload').uploadifive({
		                'formData'     : {},
		                'uploadScript' : '<?php echo PUBLIC_DIRNAME ;?>/assets/js/uploadifive/uploadifive.php'
		            });
		        });
		    </script> 
			
		</div>
		<?php
		
		return ob_get_clean();
	}
	
	public function sosa_select_domain_shortcode( $atts ){
		$sTaxonomyName = 'sosa_question_taxonomy';

		$args = array(
		    'orderby'       => 'name', 
		    'order'         => 'ASC',
		    'hide_empty'    => false, 
		    'exclude'       => array(), 
		    'exclude_tree'  => array(), 
		    'include'       => array(),
		    'number'        => '', 
		    'fields'        => 'all', 
		    'slug'          => '', 
		    'parent'         => 0,
		    'hierarchical'  => true, 
		    'child_of'      => 0, 
		    'get'           => '', 
		    'name__like'    => '',
		    'pad_counts'    => false, 
		    'offset'        => '', 
		    'search'        => '', 
		    'cache_domain'  => 'core'
		);
		
		$aTerms = get_terms($sTaxonomyName, $args);
		// top-level only -- parent terms
		
		$options = get_option( 'sosa_settings_ar' );
		$aPluginPages = $options['plugin_pages'];
		if ( $aPluginPages['step2']['parent'] ){
			$oTargetPage = get_page_by_path( $aPluginPages['step2']['parent'] . '/' . $aPluginPages['step2']['slug'] );
		} else {
			$oTargetPage = get_page_by_path( $aPluginPages['step2']['slug'] );
		}
		if ( ! $oTargetPage ) return;
		$sTargetPageLink = get_permalink( $oTargetPage->ID );
		
		ob_start();
		?>
		<form id="selector" action="<?php echo $sTargetPageLink ?>" method="POST">
		<?php wp_nonce_field( 'wp_nonce', 'sosarchitectes' ); ?>
		<input id="selected_value" name="selected_domain_id" type="hidden" value=""/>
		<?php
		foreach( $aTerms as $term ){
		?>
			<div id="block-domain-<?php echo $term->term_id;?>" class="block-domain">
				<h3><?php echo $term->name;?></h3>
				<p><?php echo $term->description;?></p>
				<?php
				// child terms
				$aSubTerms = get_term_children( $term->term_id, $sTaxonomyName );
				if ($aSubTerms){
				?>
				<select name="selected_domain[]" class="domain-selection">
					<option value="" selected="selected"><?php _e('Select your domain', 'pik-sosarchitectes'); ?></option>
				<?php
					foreach ($aSubTerms as $iSubTerm){
						$oSubTerm = get_term_by( 'id', $iSubTerm, $sTaxonomyName );
				?>
						<option value="<?php echo $iSubTerm; ?>"><?php echo $oSubTerm->name; ?></option>
				<?php
					}
				?>
				</select>
				<?php
				}
				?>
			</div>
			<script>
				jQuery(document).ready(function ($) {
					$('#selector select').change(function(){
				        if ($(this).val() != ""){
				        	$('#selected_value').val( $(this).val() );
				        	$('form#selector').submit();
						}   
					});
				});
			</script>
		<?php
		}
		?>
		</form>
		<?php
		return ob_get_clean();
	}
	
	public function sosa_user_block_shortcode( $atts ){
		extract( shortcode_atts( array(
			'id' 		=> '',
			'class' 	=> '',
			'separator'	=> '|'
		), $atts ) );
		
		$options = get_option( 'sosa_settings_ar' );
		$aPluginPages = $options['plugin_pages'];
		$oAccountPage = get_page_by_path( $aPluginPages['account']['slug'] );
		if ( ! $oAccountPage ) return;
		
		
		$sId = ($id == '') ? ' ' : ' id="' . $id . '"';
		$sClass = ($class == '') ? ' class="sosa_user_block"' : ' class="sosa_user_block ' . $class . '"';
		
		$output = '<div' . $sId . $sClass . '>'
				. '<a href="' . get_permalink($oAccountPage) . '">'
				. __('My Account', 'pik-sosarchitectes') /*same page*/ 
				. '</a>'
				. $separator;
		if ( is_user_logged_in() ) {
			$output .= '<a href="' . wp_logout_url( home_url() ) . '">'
			 		. __('Logout', 'pik-sosarchitectes')
					. '</a>';
		} else {
			$output .= '<a class="open_modal" href="#modal_loginForm">'
					. __('Login', 'pik-sosarchitectes') 
					. '</a>';
		}
		$output .= '</div>'
		
				. '<div id="modal_loginForm" class="white-popup mfp-hide">'
				. 	'<div class="column_left">'
				. 		'<p>' . __('Already registered ?', 'pik-sosarchitectes') . '</p>'
				. 		wp_login_form( array( 'echo' => false, 'remember' => false ) )
				.		'<a href="' . wp_lostpassword_url() . '">' . __('Lost password', 'pik-sosarchitectes') . '</a>'
				.	'</div>'
				. 	'<div class="column_right">'
				.		'<p>' . __('Want to Register ?', 'pik-sosarchitectes') . '</p>'
				.		'<a class="modal_btn" href="' . get_permalink($oAccountPage) . '">' . __('Register', 'pik-sosarchitectes') . '</a>'	
				.	'</div>'
				. '</div>';
		
		return $output;	
	}
	
	// END SHORTCODES
	
	
	// GRAVITY FORMS
	public function sosa_populate_pricerules($form){
		
		// get plugin option -- retrieve Prices rules
		$aSOSAOptions = get_option( 'SOSA_MenuPages' );
		$sFormName = $aSOSAOptions['sosa_settings_diverse']['question_form'];
		$sFieldInputName = $aSOSAOptions['sosa_settings_diverse']['pricerules_dynamic_field'];
		$aPriceRules = $aSOSAOptions['sosa_settings_pricerules'];
		
		if ($form['title'] != $sFormName) return $form;
		
		foreach($form['fields'] as &$field){
	        
	        if($field['inputName'] != $sFieldInputName) continue;
	        
	        $iInputID = 1;
	        $choices = array();
	        $inputs= array();
	        foreach ($aPriceRules as $aRule){
	        	$sDisplayedText = $aRule['name'] . ' | ' . $aRule['price'] . '€ HT';
				$choices[] = array( 
					'text' 			=> $sDisplayedText,
					'value' 		=> $aRule['price'],
					'isSelected'	=> false,
					'price'			=> ''
				);
				
				$inputs[] = array( 
					'label' 		=> $aRule['price'],
					'id' 			=> $field['id'] . '.' . $iInputID 
				);
				$iInputID++;
			}
			
			$field['choices'] = $choices;
			// data not sent when used -- set at 'null'
			$field['inputs'] = null;//$inputs;
	    
	    }
	    
		return $form;
	}
	
	public function sosa_after_submission($entry, $form){
		/*
		$options = get_option( 'sosa_settings_ar' );
		$aPluginPages = $options['plugin_pages'];
		if ( $aPluginPages['step3']['parent'] ){
			$oTargetPage = get_page_by_path( $aPluginPages['step3']['parent'] . '/' . $aPluginPages['step3']['slug'] );
		} else {
			$oTargetPage = get_page_by_path( $aPluginPages['step3']['slug'] );
		}
		if ( ! $oTargetPage ) return;
		$sTargetPageLink = get_permalink( $oTargetPage->ID );
		wp_redirect($sTargetPageLink);
    	exit;
    	*/
		// create Question
		var_dump($entry);
		var_dump($form);
		
		
		/* modify this for correct entries id
		category id: 7 (string)
		price: 11 (string)
  		title: 2 (strin)
  		question text: 3 (string)
		*/
		
		
		
		
	}
	
	// END GRAVITY FORMS

}
