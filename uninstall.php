<?php
/**
 * Fired when the plugin is uninstalled.
 *
 * @package   piK - SOS Architectes
 * @author    Fred Pastel <contact@frederic-pastel.com>
 * @license   GPL-2.0+
 * @link      http://example.com
 * @copyright 2014 Fred Pastel - piK
 */

// If uninstall not called from WordPress, then exit
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}

$options = get_option( 'sosa_settings_ar' );

// delete created Pages
$aPluginPages = $options['plugin_pages'];
foreach ($aPluginPages as $page){
	
	$oPage = get_page_by_path( $page['slug'] );
	if ( $oPage ) {
	    $iPageID = $oPage->ID;
	    wp_delete_post( $iPageID, true );
	}

}

delete_option( 'sosa_settings_ar' );