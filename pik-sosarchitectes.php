<?php
 /**
 * @package piK-sosarchitectes
 * @author Fred Pastel <contact@frederic-pastel.com>
 * @license GPL-2.0+
 * @link https://bitbucket.org/fredpiK/piK-sosarchitectes
 * @copyright 2014 Fred Pastel - piK
 */

/*
Plugin Name: SOS Architectes
Plugin URI: https://bitbucket.org/fredpiK/pik-sosarchitectes
Bitbucket Plugin URI: fredpiK/pik-sosarchitectes
Bitbucket Branch: master
Bitbucket Timeout: 1
Version: 1.4
Description: Description
Domain Path: /languages
Text Domain: pik-sosarchitectes
*/

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/*----------------------------------------------------------------------------*
 * Public-Facing Functionality
 *----------------------------------------------------------------------------*/

require_once( plugin_dir_path( __FILE__ ) . 'public/class-sosarchitectes.php' );

include_once( dirname( __FILE__ ) . '/includes/admin-page-framework.min.php' );
/*
 * Register hooks that are fired when the plugin is activated or deactivated.
 * When the plugin is deleted, the uninstall.php file is loaded.
 *
 */
register_activation_hook( __FILE__, array( 'Sosarchitectes', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'Sosarchitectes', 'deactivate' ) );


add_action( 'plugins_loaded', array( 'Sosarchitectes', 'get_instance' ) );

/*----------------------------------------------------------------------------*
 * Dashboard and Administrative Functionality
 *----------------------------------------------------------------------------*/
/*
 * If you want to include Ajax within the dashboard, change the following
 * conditional to:
 *
 * if ( is_admin() ) {
 *   ...
 * }
 *
 * The code below is intended to to give the lightest footprint possible.
 */
if ( is_admin() && ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) ) {

	require_once( plugin_dir_path( __FILE__ ) . 'admin/class-sosarchitectes-admin.php' );
	add_action( 'plugins_loaded', array( 'Sosarchitectes_Admin', 'get_instance' ) );

}