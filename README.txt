=== SOS Architectes ===
Contributors: Fred P.
Donate link: 
Tags: question, answer, knowledge base
Requires at least: 3.8
Tested up to: 3.8
Stable tag: 1.4
Version: 1.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Provide an interface for Question & Paid Answer process. Questions can be used as knowledge base.

== Description ==

This is the long description.  No limit, and you can use Markdown (as well as in the following sections).

For backwards compatibility, if this section is missing, the full length of the short description will be used, and
Markdown parsed.

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

= Using The WordPress Dashboard =

1. Navigate to the 'Add New' in the plugins dashboard
2. Search for 'piK - SOS Architectes'
3. Click 'Install Now'
4. Activate the plugin on the Plugin dashboard

= Uploading in WordPress Dashboard =

1. Navigate to the 'Add New' in the plugins dashboard
2. Navigate to the 'Upload' area
3. Select `piK-sosarchitectes.zip` from your computer
4. Click 'Install Now'
5. Activate the plugin in the Plugin dashboard

= Using FTP =

1. Download `piK-sosarchitectes.zip`
2. Extract the `piK-sosarchitectes` directory to your computer
3. Upload the `piK-sosarchitectes` directory to the `/wp-content/plugins/` directory
4. Activate the plugin in the Plugin dashboard


== Frequently Asked Questions ==

= A question that someone might have =

An answer to that question.

= What about foo bar? =

Answer to foo bar dilemma.

== Screenshots ==

1. This screen shot description corresponds to screenshot-1.(png|jpg|jpeg|gif). Note that the screenshot is taken from
the /assets directory or the directory that contains the stable readme.txt (tags or trunk). Screenshots in the /assets
directory take precedence. For example, `/assets/screenshot-1.png` would win over `/tags/4.3/screenshot-1.png`
(or jpg, jpeg, gif).
2. This is the second screen shot

== Changelog ==

See CHANGES.md

== Shortcodes ==

= Adding login/logout block =

`[sosa_user_block]`
Display Register/Login when not logged in | Display My Account/Logout when logged in.
Parameters (optional): 
* id : add id marker
* class : add class marker. The class 'sosa_user_block' is always declared.
* separator : character or string between the two links. (Default '|')

= Display Domain/Category selector = 

`[sosa_select_domain]`
Display a block for each Term of the taxonomy with a selector field for Subterm.
When selecting, user is send to next page.
This shorcode is used by default on Page 'Step 1'.

= Display Question Form = 

`[sosa_question_form]`
Need $_POST['selected_domain_id'] to work.
Display the Question form page.
Display blocks: Description / User data / Question Form
This shortcode is used by default on Page 'Step 2'.



== A brief Markdown Example ==

Ordered list:

1. Some feature
1. Another feature
1. Something else about the plugin

Unordered list:

* something
* something else
* third thing

Here's a link to [WordPress](http://wordpress.org/ "Your favorite software") and one to [Markdown's Syntax Documentation][markdown syntax].
Titles are optional, naturally.

[markdown syntax]: http://daringfireball.net/projects/markdown/syntax
            "Markdown is what the parser uses to process much of the readme file"

Markdown uses email style notation for blockquotes and I've been told:
> Asterisks for *emphasis*. Double it up  for **strong**.

`<?php code(); // goes in backticks ?>`
