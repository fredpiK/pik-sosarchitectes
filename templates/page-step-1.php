<?php
/*
Template Name: Step 1
*/

get_header(); ?>

<div id="main-content" class="main-content">

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post();
			?>
			
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				
				<header class="entry-header">
					<h1 class="entry-title">
					<?php
						if ( is_user_logged_in() ) {
						
							the_title();
							
						} else {
							
							_e('Please register', 'pik-sosarchitectes');
							 
						}
					?>
					</h1>
				</header>
				
				<div class="entry-content">
					<?php
						
						if ( is_user_logged_in() ) {
						
							the_content();
							// allow shortcode in the content and some text
							
						} else {
							
							echo 'not logged';
							 
						}
					
					?>
					<p>Page 'step-1' (Posez votre question)| piK-sosarchitectes</p>
					
				</div><!-- .entry-content -->
						
			</article><!-- #post-## -->

		<?php
			endwhile;
		?>
		
		</div><!-- #content -->
	</div><!-- #primary -->

</div><!-- #main-content -->

<?php
get_sidebar();
get_footer();
