<?php
/*
Template Name: Step 2
*/

get_header(); ?>

<div id="main-content" class="main-content">

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php
				//var_dump($_REQUEST);
				
				if ( ! isset( $_POST['sosarchitectes'] ) ) die( 'Bad landing');
				 
				if ( ! wp_verify_nonce( $_POST['sosarchitectes'], 'wp_nonce' ) ){
				    // This nonce is not valid.
				    die( 'Security check' ); 
				}
				
				$sTaxonomy = 'sosa_question_taxonomy';
				
				if ( $_POST['selected_domain_id'] ){
					$iCatID = $_POST['selected_domain_id'];
					$oTerm = get_term( $iCatID, $sTaxonomy );
					$oParentTerm = get_term( $oTerm->parent, $sTaxonomy );
				}
				
				// Start the Loop.
				while ( have_posts() ) : the_post();
			?>
			
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				
				<header class="entry-header">
					<h1 class="entry-title">
					<?php
						if ( is_user_logged_in() ) {
						
							echo $oParentTerm->name . ' - ' . $oTerm->name;
							
						} else {
							
							_e('Please register', 'pik-sosarchitectes');
							 
						}
					?>
					</h1>
				</header>
				
				<div class="entry-content">
					<?php
						
						if ( is_user_logged_in() ) {
							
							the_content();
							
							// allow shortcode in the content and some text
							
						} else {
							
							echo 'not logged';
							 
						}
					
					?>
					<p>Page 'step-2' (Formulaire)| piK-sosarchitectes</p>
					
				</div><!-- .entry-content -->
						
			</article><!-- #post-## -->

		<?php
			endwhile;
		?>
		
		</div><!-- #content -->
	</div><!-- #primary -->

</div><!-- #main-content -->

<?php
get_sidebar();
get_footer();
