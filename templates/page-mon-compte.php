<?php
/*
Template Name: Account
*/

get_header(); ?>

<div id="main-content" class="main-content">

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
		
		<?php //echo do_shortcode('[sosa_user_block]'); /* for test purpose */ ?>

			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post();
			?>

					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						
						<header class="entry-header">
							<h1 class="entry-title">
								<?php 
								if ( is_user_logged_in() ) {
									the_title();
								} else {
									_e('Register', 'pik-sosarchitectes');
								}
								?>
							</h1>
						</header>

						<div class="entry-content">
							<?php
								
								//the_content();
								
								if ( is_user_logged_in() ) {
								
									if ( shortcode_exists('upme') ){
										echo do_shortcode('[upme]');
									}
									
								} else {
									
									echo 'not logged';
									 
								}
							?>
							<p>Page 'mon-compte' | piK-sosarchitectes</p>
							
						</div><!-- .entry-content -->
					
					</article><!-- #post-## -->

			<?php
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) {
						comments_template();
					}
				endwhile;
			?>

		</div><!-- #content -->
	</div><!-- #primary -->
</div><!-- #main-content -->

<?php
get_sidebar();
get_footer();
