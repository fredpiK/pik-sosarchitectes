<?php
class SOSA_MenuPages extends K_AdminPageFramework {
	
	public function setUp() {
		
		/*$this->setRootMenuPage( 
			__( 'SOS Architectes', 'pik-sosarchitectes' ),
			version_compare( $GLOBALS['wp_version'], '3.8', '>=' ) ? 'dashicons-groups' : null	// dash-icons are supported since WordPress v3.8
		);*/
		
		$this->setRootMenuPageBySlug( 'edit.php?post_type=sosa_question' );
		
		$this->addSubMenuItems(
			array(
				'title' => __( 'Settings', 'pik-sosarchitectes' ),
				'page_slug' => 'sosa_settings_page',
			)
		);
		
		$this->setPageHeadingTabsVisibility( false );		// disables the page heading tabs by passing false.
		
		// tabs in Settings page
		$this->setInPageTabTag( 'h2' );
		$this->addInPageTabs(
			'sosa_settings_page',
			array(
				'tab_slug'	=>	'options',
				'title'		=>	__( 'Options', 'pik-sosarchitectes' )
			),
			array(
				'tab_slug'	=>	'saved_data',
				'title'		=>	__('Saved Data', 'pik-sosarchitectes' )
			)
		);
		
		// reset option in Options page
		$this->addSettingSections(
			'sosa_settings_page',
			array(
				'tab_slug'		=>	'saved_data',
				'section_id'	=>	'sosa_settings_reset',
				'title'			=>	__( 'Reset settings', 'pik-sosarchitectes' ),
				'description'	=>	__( 'All saved settings will be permanently deleted. Don\'t use this functionnality if you\'re not aware of what you\'re doing.' , 'pik-sosarchitectes' )
			)
		);
		$this->addSettingFields(
			array(
				'section_id'	=> 	'sosa_settings_reset',
				'field_id'		=>	'reset',
				'title'			=>	__( 'Reset options', 'pik-sosarchitectes' ),
				'type'			=>	'submit',
				'label'			=>	__( 'Reset', 'pik-sosarchitectes' ),
				'reset'			=>	true,
				'attributes'	=>	array( 'class'	=>	'button button-secondary' )
			)
		);
		
		// options fields in Options page
		$this->addSettingSections(
			'sosa_settings_page',	
			array(
				'section_id'		=>	'sosa_settings_diverse',
				'tab_slug'			=>	'options',	
				'title'				=>	__( 'Price rules settings', 'pik-sosarchitectes' ),
				'description'		=>	__( 'Administrator settings', 'pik-sosarchitectes' ),
			),
			array(
				'section_id'		=>	'sosa_settings_pricerules',
				'tab_slug'			=>	'options',	
				'title'				=>	__( 'Price rules', 'pik-sosarchitectes' ),
				'description'		=>	__( 'Define a price rule for customer choice', 'pik-sosarchitectes' ),
				'repeatable'		=>	array('min' => 1, 'max' => 5 )
			),
			array(
				'section_tab_slug'	=> ''
			),
			array(
				'section_id'		=>	'sosa_settings_submit',
				'title'				=>	__( 'Save settings', 'pik-sosarchitectes' ),
			)			
		);
		$this->addSettingFields(
			array(
	    		'field_id'		=>	'question_form',
				'section_id'	=>	'sosa_settings_diverse',
				'title'			=>	__( 'Name of Dynamic Form', 'pik-sosarchitectes' ),
				'type'			=>	'text',
				'default'		=>	'sosa_question_form'
	    	),
	    	array(
	    		'field_id'		=>	'pricerules_dynamic_field',
				'section_id'	=>	'sosa_settings_diverse',
				'title'			=>	__( 'Name of Dynamic Form Field for PriceRules', 'pik-sosarchitectes' ),
				'type'			=>	'text',
				'default'		=>	'sosa_pricerules'
	    	),
	    	array(
	    		'field_id'		=>	'pricerules_tax_rate',
				'section_id'	=>	'sosa_settings_diverse',
				'title'			=>	__( 'Tax rate (VAT)', 'pik-sosarchitectes' ),
				'type'			=>	'text',
				'default'		=> '20',
				'after_input'	=>	' %'
	    	)
	    );
		$this->addSettingFields(
	        array(
                'section_id'	=>	'sosa_settings_pricerules',
                'field_id'      =>	'name',
                'title' 		=>	__( 'Delay', 'pik-sosarchitectes' ),
                'description'   =>	__( 'Name explicitly the delay', 'pik-sosarchitectes' ),
                'type'  		=>  'text',
                'order' 		=>	1,
                'default'       =>	'Name',
                'size' 			=>	40,
	        ),
	        array(
                'section_id'	=>	'sosa_settings_pricerules',
                'field_id'      =>	'price',
                'title' 		=>	__( 'Price', 'pik-sosarchitectes' ),
                'description'   =>	__( 'Price for this delay', 'pik-sosarchitectes' ),
                'type'  		=>  'number',
                'order' 		=>	2,
                'default'       =>	0,
                'size' 			=>	40,
                'after_input'	=>	' &euro; HT'
	        ),
	        array()
	    );
	    $this->addSettingFields(
	    	array(	// Submit Button
				'field_id'		=>	'submit',
				'section_id'	=>	'sosa_settings_submit',
				'title'			=>	__( 'Save options', 'pik-sosarchitectes' ),
				'type'			=>	'submit'
			)
	    );
	}	
	
	public function do_sosa_settings_page_saved_data() {  // do_ + {page slug} + {tab_slug}
		
		?>
		<h3><?php _e( 'Saved Data', 'pik-sosarchitectes' ); ?></h3>
		<p>
		<?php 
			echo sprintf( __( 'To retrieve the saved option values simply you can use the WordPress <code>get_option()</code> function. The key is the extended class name by default unless it is specified in the constructor. In this demo plugin, <code>%1$s</code>, is used as the option key.', 'admin-page-framework-demo' ), $this->oProp->sOptionKey );
			echo ' ' . sprintf( __( 'It is stored in the <code>$this->oProp-sOptionKey</code> class property so you may access it directly to confirm the value. So the required code would be <code>get_option( %1$s );</code>.', 'admin-page-framework-demo' ), $this->oProp->sOptionKey );
			echo ' ' . __( 'If you are retrieving them within the framework class, simply call <code>$this->oProp->aOptions</code>.', 'admin-page-framework-demo' );
		?>
		</p>
		<p>
		<?php
			echo __( 'Alternatively, there is the <code>AdminPageFramework::getOption()</code> static method. This allows you to retrieve the array element by specifying the option key and the array key (field id or section id).', 'admin-page-framework-demo' );
			echo ' ' . __( 'Pass the option key to the first parameter and an array representing the dimensional keys to the second parameter', 'admin-page-framework-demo' );
			echo ' ' . __( '<code>$aData = AdminPageFramework::getOption( \'APF_Demo\', array( \'text_fields\', \'text\' ), \'default value\' );</code> will retrieve the option array value of <code>$aArray[\'text_field\'][\'text\']</code>.', 'admin-page-framework-demo' );	
			echo ' ' . __( 'This method is merely to avoid multiple uses of <code>isset()</code> to prevent PHP warnings.', 'admin-page-framework-demo' );
			echo ' ' . __( 'So if you already know how to retrieve a value of an array element, you don\'t have to use it.', 'admin-page-framework-demo' );	// ' syntax fixer
		?>
		</p>
		<?php
			echo $this->oDebug->getArray( $this->oProp->aOptions ); 
			// echo $this->oDebug->getArray( AdminPageFramework::getOption( 'APF_Demo', array( 'text_fields' ) ) );
	}
	
}