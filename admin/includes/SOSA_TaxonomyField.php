<?php
class SOSA_TaxonomyField extends K_AdminPageFramework_TaxonomyField {
		
	/*
	 * ( optional ) Use the setUp() method to define settings of this meta box.
	 */
	public function setUp() {

		/*
		 * ( optional ) Adds setting fields into the meta box.
		 */
		//$this->addSettingFields();			
		
	}
	
	/*
	 * ( optional ) modify the columns of the term listing table
	 */
	public function columns_SOSA_TaxonomyField( $aColumn ) {	// column_{extended class name}
		
		//return array( 'cb' => $aColumn['cb'], 'thumbnail' => __( 'Thumbnail', 'pik-sosarchitectes' ) ) + $aColumn;
		//return $aColumn;
		
	}

	/*
	 * ( optional ) output the stored option to the custom column
	 */	
	public function cell_SOSA_TaxonomyField( $sCellHTML, $sColumnSlug, $iTermID ) {	// cell_{extended class name}
		
		/*
		if ( ! $iTermID || $sColumnSlug != 'thumbnail' ) return $sCellHTML;
		
		$aOptions = get_option( 'SOSA_TaxonomyField', array() );	// by default the class name is the option key.
		return isset( $aOptions[ $iTermID ][ 'image_upload' ] ) && $aOptions[ $iTermID ][ 'image_upload' ]
			? "<img src='{$aOptions[ $iTermID ][ 'image_upload' ]}' style='max-height: 72px; max-width: 120px;'/>"
			: $sCellHTML;
		*/
		//return $sCellHTML;
		
	}
	
	/*
	 * ( optional ) Use this method to insert your custom text.
	 */
	public function do_SOSA_TaxonomyField() {	// do_{extended class name}
		?>
			<p><?php _e( 'This text is inserted with the <code>do_{extended class name}</code> hook.', 'admin-page-framework-demo' ) ?></p>
		<?php		
	}

	/*
	 * ( optional ) Use this method to validate submitted option values.
	 */
	public function validation_SOSA_TaxonomyField( $aNewOptions, $aOldOptions ) {

		// Do something to compare the values.
		//return $aNewOptions;
	}
	
}